/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file
 */


/**
 * The routes
 *
 * @type {object} The routes
 */
export default [
  // Login
  {
    path: '/login',
    name: 'login.index',
    component: require('./components/login/login.vue'),
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: require('./components/register/register.vue'),
  },
  {
    path: '/settings',
    name: 'settings.index',
    component: require('./components/settings/settings.vue'),
  },
  {
    path: '/editor/:slug?',
    name: 'editor.index',
    component: require('./components/article/article-editor/article-editor.vue'),
  },
  {
    path: '/',
    name: 'home.index',
    component: require('./components/home/home.vue'),
  },
  {
    path: '/article/:slug',
    name: 'article.index',
    component: require('./components/article/article.vue'),
  },
  {
    path: '/profile/:username',
    name: 'profile.index',
    component: require('./components/profile/profile.vue'),
  },
];
