import profileService from './../../services/profile';
import articleService from './../../services/article';
import { mapState } from 'vuex';
import articleList from './../article/article-list/article-list.vue';

export default {
  created() {
    const username = this.$route.params.username;
    profileService.loaded(Promise.all([profileService.get(username),
      articleService.getBy(username)]));
  },

  methods: {
    myHandler() {
      profileService.changeTab('your', articleService.getBy(this.profile.username));
    },
    favHandler() {
      profileService.changeTab('favorite', articleService.favBy(this.profile.username));
    },
    clicked(page) {
      const promise = this.tab === 'your' ? articleService.getByWithPage(page, this.profile.username)
        : articleService.favByWithPage(page, this.profile.username);
      profileService.changePage(page, promise);
    },

    toggleFollowing(profile) {
      profileService.toggleFollowing(profile);
    },
  },

  destroyed() {
    profileService.destroy();
  },

  computed: {
    ...mapState({
      articleList: state => state.profile.articles,
      pagesList: state => state.profile.pagesList,
      tab: state => state.profile.tab,
      currentPage: state => state.profile.currentPage,
      profile: state => state.profile.profile,
      currentUser: state => state.auth.currentUser,
    }),

    isUser() {
      return this.currentUser ? this.currentUser.username === this.$route.params.username : false;
    },

    followClass() {
      const cssClass = ['btn', 'btn-sm', 'action-btn'];
      if (!this.profile.following) {
        cssClass.push('btn-outline-secondary');
      } else {
        cssClass.push('btn-secondary');
      }
      return cssClass;
    },

    favClass() {
      const cssClass = ['nav-link'];
      if (this.tab === 'favorite') {
        cssClass.push('active');
      }
      return cssClass;
    },
    myClass() {
      const cssClass = ['nav-link'];
      if (this.tab === 'your') {
        cssClass.push('active');
      }
      return cssClass;
    },
  },

  components: {
    'article-list': articleList,
  },
};
