import authService from '../../services/auth';
import listErrors from '../errors/listErrors.vue';

export default {

  data() {
    return {
      user: {
        email: null,
        password: null,
      },
      errors: {},
    };
  },

  methods: {
    login(user) {
      authService.login(user)
        .then(() => this.$set(this, 'errors', this.$store.state.auth.errors));
    },
  },
  components: {
    'list-errors': listErrors,
  },
};
