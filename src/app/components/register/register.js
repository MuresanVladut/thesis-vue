import authService from '../../services/auth';
import listErrors from '../errors/listErrors.vue';

export default {

  data() {
    return {
      user: {
        username: null,
        email: null,
        password: null,
      },
      errors: {},
    };
  },

  methods: {
    register(user) {
      authService.register(user)
        .then(() => this.$set(this, 'errors', this.$store.state.auth.errors));
    },
  },
  components: {
    'list-errors': listErrors,
  },
};
