export default {
  props: ['tags', 'clickTag'],

  methods: {
    handleClick(tag) {
      this.clickTag(tag);
    },
  },
};
