import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState({
      appName: state => state.common.appName,
    }),
  },
};
