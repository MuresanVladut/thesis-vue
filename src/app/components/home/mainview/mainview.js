import articleList from './../../article/article-list/article-list.vue';
import globalFeedTab from './global-feed-tab/global-feed-tab.vue';
import yourFeedTab from './your-feed-tab/your-feed-tab.vue';
import tagFilterTab from './tag-filter-tab/tag-filter-tab.vue';
import homeService from './../../../services/home';
import tagsService from './../../../services/tags';
import { mapState } from 'vuex';

export default {
  components: {
    'article-list': articleList,
    'global-feed-tab': globalFeedTab,
    'your-feed-tab': yourFeedTab,
    'tag-filter-tab': tagFilterTab,
  },

  methods: {
    clicked(page) {
      if (this.tag) {
        homeService.changePage(page, tagsService.applyTagWithPage(this.tag, page));
      } else {
        const promise = this.tab === 'feed' ? homeService.feedWithPage(page)
          : homeService.allWithPage(page);
        homeService.changePage(page, promise);
      }
    },
  },

  computed: {
    ...mapState({
      articleList: state => state.articleList.articles,
      pagesList: state => state.articleList.pagesList,
      tab: state => state.articleList.tab,
      currentPage: state => state.articleList.currentPage,
      tag: state => state.articleList.tag,
    }),
  },
};
