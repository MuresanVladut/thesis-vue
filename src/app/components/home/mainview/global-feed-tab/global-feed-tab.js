import homeService from './../../../../services/home';
import articleService from './../../../../services/article';

export default {
  props: ['tab'],
  methods: {
    clickHandler() {
      homeService.changeTab('all', articleService.all());
    },
  },
  computed: {
    globalClass() {
      const cssClasses = ['nav-link'];
      if (this.tab === 'all') cssClasses.push('active');
      return cssClasses;
    },
  },
};
