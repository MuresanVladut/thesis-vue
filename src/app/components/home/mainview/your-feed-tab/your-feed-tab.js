import homeService from './../../../../services/home';
import articleService from './../../../../services/article';

export default {
  props: ['tab'],
  methods: {
    clickHandler() {
      homeService.changeTab('feed', articleService.feed());
    },
  },
  computed: {
    feedClass() {
      const cssClasses = ['nav-link'];
      if (this.tab === 'feed') cssClasses.push('active');
      return cssClasses;
    },
  },
};
