import banner from './banner/banner.vue';
import mainView from './mainview/mainview.vue';
import tagsFilter from './tags-filter/tags-filter.vue';
import { mapState } from 'vuex';
import articleService from './../../services/article';
import tags from './../../services/tags';
import homeService from './../../services/home';
import comments from './../../services/comments';
import latestComments from './latest-comments/latest-comments.vue';
import recommendations from './recommendations/recommendations.vue';

export default {
  components: {
    'banner-cmp': banner,
    'main-view': mainView,
    'tags-filter': tagsFilter,
    'latest-comments': latestComments,
    'article-suggestions': recommendations,
  },

  methods: {
    onClickTag(tag) {
      tags.applyTag(tag);
    },
  },

  created() {
    const tab = this.authenticated ? 'feed' : 'all';
    const articlePromise = this.authenticated ? articleService.feed() : articleService.all();
    if (!this.authenticated) {
      homeService.loaded(tab, Promise.all([articlePromise, tags.all(), comments.latest()]));
    } else {
      homeService.loadedWithRecommendations(tab,
        Promise.all([articlePromise, tags.all(), comments.latest(),
          articleService.recommendations()]));
    }
  },

  destroyed() {
    homeService.unloaded();
  },

  computed: {
    ...mapState({
      authenticated: state => state.auth.authenticated,
      tags: state => state.articleList.tags,
      comments: state => state.articleList.comments,
      recommendations: state => state.articleList.recommendations,
    }),
  },
};
