export default {
  data() {
    return {
      settings: {
        old_password: null,
        image: this.$store.state.auth.currentUser ? this.$store.state.auth.currentUser.image : null,
        username: this.$store.state.auth.currentUser
          ? this.$store.state.auth.currentUser.username : null,
        bio: this.$store.state.auth.currentUser
          ? this.$store.state.auth.currentUser.bio : null,
        email: this.$store.state.auth.currentUser
          ? this.$store.state.auth.currentUser.email : null,
        password: null,
      },
    };
  },

  props: ['submitForm'],
  methods: {
    saveSettings(settings) {
      if (this.$parent[this.submitForm]) {
        this.$parent[this.submitForm].call(this, settings);
      }
    },
  },
};
