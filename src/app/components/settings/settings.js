import authService from '../../services/auth';
import settingsForm from './settingsForm/settingsForm.vue';
import settingsService from '../../services/settings';
import listErrors from '../errors/listErrors.vue';
import { mapState } from 'vuex';

export default {
  data() {
    return {
      errors: {},
    };
  },
  computed: {
    ...mapState({
      currentUser: state => state.auth.currentUser,
    }),
  },
  methods: {
    logout() {
      authService.logout();
    },
    submitForm(settings) {
      const user = Object.assign({}, this.currentUser, settings);
      settingsService.update(user)
        .then(() => this.$set(this, 'errors', this.$store.state.auth.errors));
    },
  },
  components: {
    'settings-form': settingsForm,
    'list-errors': listErrors,
  },
};
