export default {
  props: ['pagesList', 'clicked', 'currentPage'],

  methods: {
    click(page) {
      this.clicked(page);
    },
    isCurrent(page) {
      const cssClass = ['page-item'];
      if (this.currentPage === page) {
        cssClass.push('active');
      }
      return cssClass;
    },
  },
};
