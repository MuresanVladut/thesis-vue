import articleService from './../../../services/article';

export default {
  props: ['article'],
  methods: {
    handleClick(article) {
      articleService.favUnfav(article);
    },
  },
  computed: {
    favorited() {
      const favorited = ['btn', 'btn-sm', 'btn-primary'];
      const notFavorited = ['btn', 'btn-sm', 'btn-outline-primary'];
      if (this.article.favorited) {
        return favorited;
      }
      return notFavorited;
    },
  },
};
