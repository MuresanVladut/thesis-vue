import comment from './../comment/comment.vue';

export default {
  props: ['comments'],
  components: {
    'comment': comment,
  },
};
