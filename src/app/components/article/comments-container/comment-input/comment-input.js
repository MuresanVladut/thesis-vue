import commentsService from './../../../../services/comments';

export default {
  data() {
    return {
      comment: {
        body: null,
      },
    };
  },

  methods: {
    createComment(comment) {
      commentsService.add(this.$route.params.slug, comment).then(
        () => {
          this.comment.body = '';
        },
      );
    },
  },
};
