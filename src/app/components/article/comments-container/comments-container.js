import commentInput from './comment-input/comment-input.vue';
import commentList from './comment-list/comment-list.vue';

export default {
  props: ['comments'],
  components: {
    'comment-input': commentInput,
    'comment-list': commentList,
  },
};
