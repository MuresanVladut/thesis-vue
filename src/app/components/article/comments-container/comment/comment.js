import commentService from './../../../../services/comments';

export default {
  props: ['comment'],
  methods: {
    deleteComment() {
      commentService.deleteComment(this.$route.params.slug, this.comment.id);
    },
  },
};
