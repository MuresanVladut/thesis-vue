import articleActions from './../article-actions/article-actions.vue';
import articleService from './../../../services/article';

export default {
  props: ['article'],
  components: {
    'article-actions': articleActions,
  },

  methods: {
    toggleFollowing(article) {
      articleService.toggleFollowing(article);
    },
    toggleFavorite(article) {
      articleService.toggleFavorite(article);
    },
  },

  computed: {
    followClass() {
      const cssClass = ['btn', 'btn-sm', 'action-btn'];
      if (!this.article.author.following) {
        cssClass.push('btn-outline-secondary');
      } else {
        cssClass.push('btn-secondary');
      }
      return cssClass;
    },
    favoriteClass() {
      const cssClass = ['btn', 'btn-sm'];
      if (!this.article.favorited) {
        cssClass.push('btn-outline-primary');
      } else {
        cssClass.push('btn-primary');
      }
      return cssClass;
    },
  },
};
