/* eslint-disable */
import articleService from '../../../services/article';
import listErrors from '../../errors/listErrors.vue';

export default {

  data() {
    return {
      article: {
        title: null,
        description: null,
        body: null,
        tag: null,
        tagList: [],
      },
      errors: {},
    };
  },

  created() {
    if (this.$route.params.slug) {
      articleService.get(this.$route.params.slug)
        .then((response) => this.article = response.data.article);
    }
  },
  methods: {
    submitForm(article) {
      this.article.tag = null;
      if(this.$route.params.slug){
        debugger;
        articleService.update(article)
          .then(() => this.$set(this, 'errors', this.$store.state.article.errors));
      } else {
        articleService.save(article)
          .then(() => this.$set(this, 'errors', this.$store.state.article.errors));
      }
    },
    watchForEnter(event) {
      const keyCode = event.key || String.fromCharCode(event.keyCode);
      if (keyCode === 'Enter') {
        this.article.tagList.push(this.article.tag);
        this.article.tag = '';
      }
    },
    removeTag(tag) {
      this.article.tagList = this.article.tagList.filter(tagg => tagg !== tag);
    },
  },
  components: {
    'list-errors': listErrors,
  },
};
