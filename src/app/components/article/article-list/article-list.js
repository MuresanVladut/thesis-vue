import articlePreview from './../article-preview/article-preview.vue';
import listPagination from './../list-pagination/list-pagination.vue';

export default {
  props: ['list', 'pagesList', 'clicked', 'currentPage'],
  components: {
    'article-preview': articlePreview,
    'list-pagination': listPagination,
  },
};
