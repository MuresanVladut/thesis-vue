/* eslint-disable */
import articleService from './../../services/article';
import commentsService from './../../services/comments';
import articleMeta from './article-meta/article-meta.vue';
import commentsContainer from './comments-container/comments-container.vue';
import store from './../../store';
import { mapState } from 'vuex';

export default {
  created() {
    store.dispatch('articleLoaded', Promise.all([
      articleService.get(this.$route.params.slug),
      commentsService.getForArticle(this.$route.params.slug)
    ]));
  },

  components: {
    'article-meta': articleMeta,
    'comments-container': commentsContainer,
  },
  computed: {
    ...mapState({
      article: state => state.article.article,
      comments: state => state.article.comments
    }),
  },

  destroyed() {
    articleService.destroyed();
  }
};
