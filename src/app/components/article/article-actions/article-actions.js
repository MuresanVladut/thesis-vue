import articleService from './../../../services/article';

export default {
  methods: {
    deleteArticle() {
      articleService.deleteArticle(this.$route.params.slug);
    },
  },
};
