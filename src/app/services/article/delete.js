/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = () => {
    Vue.router.push({
      name: 'home.index',
    });
};

export default (slug) => {
  return Vue.$http.delete(`/articles/${slug}`)
    .then(() => {
      dispatch();
    });
};
