/* eslint-disable */

import Vue from 'vue';

export default (slug) => {
  return Vue.$http.get(`/articles/${slug}`);
};
