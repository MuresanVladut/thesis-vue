/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload, favorited) => {
  if(favorited){
    store.dispatch('favorite', payload);
  } else {
    store.dispatch('unfavorite', payload);
  }
};

export default (article) => {
  if(article.favorited){
    Vue.$http.delete(`/articles/${article.slug}/favorite`)
      .then((response) => {
        dispatch(response.data.article, false);
      });
  } else {
    Vue.$http.post(`/articles/${article.slug}/favorite`)
      .then((response) => {
        dispatch(response.data.article, true);
      });
  }
};
