/* eslint-disable */

import Vue from 'vue';

export default (username) => {
  return Vue.$http.get(`/articles?favorited=${username}&limit=10&offset=0`);
}
