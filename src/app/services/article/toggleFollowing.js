/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('toggleFollowing', payload);
};

export default (article) => {
  if(article.author.following){
    Vue.$http.delete(`/profiles/${article.author.username}/follow`)
      .then((response) => {
        dispatch(response.data);
      });
  } else {
    Vue.$http.post(`/profiles/${article.author.username}/follow`)
      .then((response) => {
        dispatch(response.data);
      });
  }
};
