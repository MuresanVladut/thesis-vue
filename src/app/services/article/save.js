/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  if (!payload.errors) {
    Vue.router.push({
      name: 'home.index',
    });
  } else {
    store.dispatch('articleEditor', payload);
  }
};

export default (article) => {
  return Vue.$http.post('/articles', { article })
    .then((response) => {
      dispatch(response.data.article);
    }).catch((error) => {
      dispatch(error.response.data);
    });
};
