/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  if (!payload.errors) {
    Vue.router.push({
      name: 'home.index',
    });
  } else {
    store.dispatch('articleEditor', payload);
  }
};

export default (article) => {
  const articleToPut = Object.assign({article}, article, {slug: undefined, author: undefined, createdAt: undefined, favorited: undefined, favoritesCount: undefined,
                tag: undefined, updatedAt: undefined});
  debugger;
  return Vue.$http.put(`/articles/${article.slug}`, articleToPut)
    .then((response) => {
      dispatch(response.data.article);
    }).catch((error) => {
      dispatch(error.response.data);
    });
};
