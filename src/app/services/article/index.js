import save from './save';
import get from './get';
import update from './update';
import all from './all';
import deleteArticle from './delete';
import destroyed from './destroyed';
import feed from './feed';
import favUnfav from './favUnfav';
import toggleFavorite from './toggleFavorite';
import toggleFollowing from './toggleFollowing';
import getBy from './getBy';
import recommendations from './recommendations';
import favBy from './favBy';
import getByWithPage from './getByWithPage';
import favByWithPage from './favByWithPage';

export default {
  save,
  all,
  get,
  deleteArticle,
  destroyed,
  feed,
  favUnfav,
  toggleFavorite,
  toggleFollowing,
  getBy,
  favBy,
  getByWithPage,
  favByWithPage,
  update,
  recommendations,
};
