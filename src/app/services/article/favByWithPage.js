/* eslint-disable */

import Vue from 'vue';

export default (page, username) => {
  return Vue.$http.get(`/articles?favorited=${username}&limit=10&offset=${page * 10}`);
}
