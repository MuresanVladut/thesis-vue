/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('toggleFavorite', payload);
};

export default (article) => {
  if(article.favorited){
    Vue.$http.delete(`/articles/${article.slug}/favorite`)
      .then((response) => {
        dispatch(response.data.article);
      });
  } else {
    Vue.$http.post(`/articles/${article.slug}/favorite`)
      .then((response) => {
        dispatch(response.data.article);
      });
  }
};
