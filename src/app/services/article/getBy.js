/* eslint-disable */

import Vue from 'vue';

export default (username) => {
  return Vue.$http.get(`/articles?author=${username}&limit=10&offset=0`);
}
