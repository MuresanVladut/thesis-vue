import login from './login';
import logout from './logout';
import register from './register';
import get from './get';

export default {
  login,
  logout,
  register,
  get,
};
