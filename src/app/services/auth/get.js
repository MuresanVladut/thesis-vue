/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('login', payload);
};

export default () => {
  return Vue.$http.get('/user')
    .then((response) => {
      dispatch(response.data.user);
    }).catch((error) => {
      dispatch(error.response.data);
    });
};
