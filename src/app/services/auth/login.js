/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('login', payload);
  if (!payload.errors) {
    Vue.router.push({
      name: 'home.index',
    });
  }
};

export default (user) => {
  return Vue.$http.post('/users/login', { user: { email: user.email, password: user.password } })
     .then((response) => {
       dispatch(response.data.user);
     }).catch((error) => {
       dispatch(error.response.data);
     });
};
