import loaded from './loaded';
import loadedWithRecommendations from './loadedWithRecommendations';
import changeTab from './changeTab';
import changePage from './changePage';
import feedWithPage from './feedWithPage';
import allWithPage from './allWithPage';
import unloaded from './unloaded';

export default {
  loaded,
  loadedWithRecommendations,
  changeTab,
  changePage,
  feedWithPage,
  allWithPage,
  unloaded,
};
