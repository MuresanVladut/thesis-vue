/* eslint-disable */

import store from './../../store';

export default (page, payload) => {
  store.dispatch('setPage', [page, payload]);
};
