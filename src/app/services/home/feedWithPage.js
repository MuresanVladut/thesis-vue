/* eslint-disable */

import Vue from 'vue';

export default (page) => {
  return Vue.$http.get(`/articles/feed?limit=10&offset=${page * 10}`);
}
