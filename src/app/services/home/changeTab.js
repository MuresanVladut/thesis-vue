/* eslint-disable */

import store from './../../store';

export default (tab, payload) => {
  store.dispatch('changeTab', [tab, payload]);
};
