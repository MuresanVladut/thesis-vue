/* eslint-disable */

import Vue from 'vue';

export default (page) => {
  return Vue.$http.get(`/articles?limit=10&offset=${page * 10}`);
};
