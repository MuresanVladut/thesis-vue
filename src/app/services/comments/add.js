/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('addComment', payload);
};

export default (slug, comment) => {
  return Vue.$http.post(`/articles/${slug}/comments`, {'comment': comment})
    .then((response) => dispatch(response.data.comment));
};
