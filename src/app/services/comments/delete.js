/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (commentId) => {
  store.dispatch('deleteComment', commentId);
};

export default (slug, commentId) => {
  return Vue.$http.delete(`/articles/${slug}/comments/${commentId}`)
    .then(dispatch(commentId));
};
