import getForArticle from './getForArticle';
import deleteComment from './delete';
import add from './add';
import latest from './latest';

export default {
  getForArticle,
  deleteComment,
  add,
  latest,
};
