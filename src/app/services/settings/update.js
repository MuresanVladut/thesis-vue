/*eslint-disable */
import Vue from 'vue';
import store from './../../store';

const dispatch = (payload) => {
  store.dispatch('settingsSaved', payload);
  if(!payload.errors) {
    Vue.router.push({
      name: 'home.index',
    });
  }
};


export default (user) => {
  return Vue.$http.put('/user', { user })
    .then((response) => {
      dispatch(response.data.user);
    })
    .catch((error) => {
      dispatch(error.response.data);
    });
};
