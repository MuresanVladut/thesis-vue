/*eslint-disable */
import Vue from 'vue';

export default (tag, page) => {
  return Vue.$http.get(`/articles?tag=${tag}&limit=10&offset=${page * 10}`);
};
