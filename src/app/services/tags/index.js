import all from './all';
import applyTag from './applyTag';
import applyTagWithPage from './applyTagWithPage';

export default {
  all,
  applyTag,
  applyTagWithPage,
};
