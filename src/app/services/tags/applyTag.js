/*eslint-disable */
import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (tag, payload) => {
  store.dispatch('applyTag', [tag, payload]);
};

export default (tag) => {
  Vue.$http.get(`/articles?tag=${tag}&limit=10&offset=0`)
    .then((response) => dispatch(tag, response.data));
};
