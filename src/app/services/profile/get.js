/* eslint-disable */

import Vue from 'vue';

export default (username) => {
  return Vue.$http.get(`/profiles/${username}`);
}
