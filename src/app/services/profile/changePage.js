/* eslint-disable */

import store from './../../store';

export default (page, payload) => {
  store.dispatch('setProfilePage', [page, payload]);
};
