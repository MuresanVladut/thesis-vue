/* eslint-disable */

import store from './../../store';

export default (tab, payload) => {
  store.dispatch('changeTabProfile', [tab, payload]);
};
