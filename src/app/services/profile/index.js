import loaded from './loaded';
import get from './get';
import changePage from './changePage';
import destroy from './destroy';
import changeTab from './changeTab';
import toggleFollowing from './toggleFollowing';

export default {
  loaded,
  get,
  changeTab,
  changePage,
  destroy,
  toggleFollowing,
};
