/* eslint-disable */

import Vue from 'vue';
import store from './../../store';

// When the request succeeds
const dispatch = (payload) => {
  store.dispatch('toggleFollowingProfile', payload);
};

export default (profile) => {
  if(profile.following){
    Vue.$http.delete(`/profiles/${profile.username}/follow`)
      .then((response) => {
        dispatch(response.data);
      });
  } else {
    Vue.$http.post(`/profiles/${profile.username}/follow`)
      .then((response) => {
        dispatch(response.data);
      });
  }
};
