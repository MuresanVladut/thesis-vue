/* eslint-disable */
import {
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
  FAVORITE,
  UNFAVORITE,
  CHANGE_TAB_PROFILE,
  SET_PAGE_PROFILE,
  TOGGLE_FOLLOWING_PROFILE,
} from './../../mutation-types';
import Vue from 'vue';

export default {
  [PROFILE_PAGE_LOADED](state, payload) {
    payload.then((response) => {
      Vue.set(state, 'articles', response[1].data.articles);
      Vue.set(state, 'articlesCount', response[1].data.articlesCount);
      const range = [];
      if(response[1].data.articlesCount > 10) {
        for (let i = 0; i < Math.ceil(response[1].data.articlesCount / 10); ++i) {
          range.push(i);
        }
      }
      Vue.set(state, 'pagesList', range);
      Vue.set(state, 'tab', 'your');
      Vue.set(state, 'currentPage', 0);
      Vue.set(state, 'profile', response[0].data.profile)
    });
  },

  [PROFILE_PAGE_UNLOADED](state) {
    Vue.set(state, 'articles', {});
    Vue.set(state, 'articlesCount', 0);
    Vue.set(state, 'pageList', {});
    Vue.set(state, 'tab', '');
    Vue.set(state, 'profile', {});
    Vue.set(state, 'currentPage', 0);
  },

  [FAVORITE](state, payload) {
    if(state.articles && state.articles.length) {
      for (let article of state.articles) {
        if (article.slug === payload.slug) {
          article.favorited = payload.favorited;
          article.favoritesCount = payload.favoritesCount;
        }
      }
    }
  },

  [UNFAVORITE](state, payload) {
    if(state.articles && state.articles.length) {
      for (let article of state.articles) {
        if (article.slug === payload.slug) {
          article.favorited = payload.favorited;
          article.favoritesCount = payload.favoritesCount;
        }
      }
    }
  },

  [CHANGE_TAB_PROFILE](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response.data.articles);
      Vue.set(state, 'articlesCount', response.data.articlesCount);
      const range = [];
      for (let i = 0; i < Math.ceil(response.data.articlesCount / 10); ++i) {
        range.push(i);
      }
      Vue.set(state, 'pagesList', range);
      Vue.set(state, 'tab', payload[0]);
      Vue.set(state, 'currentPage', 0);
    });
  },

  [SET_PAGE_PROFILE](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response.data.articles);
      Vue.set(state, 'articlesCount', response.data.articlesCount);
      Vue.set(state, 'currentPage', payload[0]);
    });
  },

  [TOGGLE_FOLLOWING_PROFILE](state, payload) {
    Vue.set(state.profile, 'following', payload.profile.following);
  },
};
