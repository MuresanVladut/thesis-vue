import Vue from 'vue';
import {
  LOGOUT,
  CHECK_AUTHENTICATION,
  LOGIN,
  SETTINGS_SAVED,
} from './../../mutation-types';

export default {
  [CHECK_AUTHENTICATION](state) {
    Vue.set(state, 'authenticated', localStorage.getItem('id_token'));
    if (state.authenticated) {
      Vue.$http.defaults.headers.common.Authorization = `Token ${localStorage.getItem('id_token')}`;
    }
  },

  [LOGIN](state, payload) {
    if (!payload.errors) {
      Vue.set(state, 'authenticated', true);
      Vue.set(state, 'currentUser', payload);
      localStorage.setItem('id_token', payload.token);
      Vue.$http.defaults.headers.common.Authorization = `Token ${payload.token}`;
    }
    state.errors = payload.errors;
  },

  [LOGOUT](state) {
    Vue.set(state, 'authenticated', false);
    Vue.set(state, 'currentUser', null);
    localStorage.removeItem('id_token');
    Vue.$http.defaults.headers.common.Authorization = '';
  },
  [SETTINGS_SAVED](state, payload) {
    if (!payload.errors) {
      Vue.set(state, 'currentUser', payload);
      localStorage.setItem('id_token', payload.token);
      Vue.$http.defaults.headers.common.Authorization = `Token ${payload.token}`;
    }
    Vue.set(state, 'errors', payload.errors);
  },
};
