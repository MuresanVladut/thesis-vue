import {
  ARTICLE_EDITOR,
  ARTICLE_PAGE_LOADED,
  DELETE_COMMENT,
  ADD_COMMENT,
  ARTICLE_DESTROYED,
  TOGGLE_FAVORITE,
  TOGGLE_FOLLOWING,
} from './../../mutation-types';
import marked from 'marked';
import Vue from 'vue';

export default {
  [ARTICLE_EDITOR](state, payload) {
    state.errors = payload.errors;
  },
  [ARTICLE_PAGE_LOADED](state, payload) {
    payload.then((response) => {
      Vue.set(state, 'article', response[0].data.article);
      Vue.set(state.article, 'marked', marked(state.article.body));
      Vue.set(state, 'comments', response[1].data.comments);
    });
  },

  [DELETE_COMMENT](state, payload) {
    Vue.set(state, 'comments', state.comments.filter(comment => comment.id !== payload));
  },

  [ADD_COMMENT](state, payload) {
    Vue.set(state, 'comments', [payload].concat(state.comments));
  },

  [ARTICLE_DESTROYED](state) {
    Vue.set(state, 'article', {});
  },

  [TOGGLE_FAVORITE](state, payload) {
    Vue.set(state.article, 'favorited', payload.favorited);
    Vue.set(state.article, 'favoritesCount', payload.favoritesCount);
  },

  [TOGGLE_FOLLOWING](state, payload) {
    Vue.set(state.article.author, 'following', payload.profile.following);
  },
};
