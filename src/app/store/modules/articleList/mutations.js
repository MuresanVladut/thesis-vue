/* eslint-disable */
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_LOADED_WITH_RECOMMENDATIONS,
  HOME_PAGE_UNLOADED,
  CHANGE_TAB,
  SET_PAGE,
  APPLY_TAG,
  FAVORITE,
  UNFAVORITE,
} from './../../mutation-types';
import Vue from 'vue';

export default {
  [HOME_PAGE_LOADED](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response[0].data.articles);
      Vue.set(state, 'articlesCount', response[0].data.articlesCount);
      const range = [];
      if(response[0].data.articlesCount > 10) {
        for (let i = 0; i < Math.ceil(response[0].data.articlesCount / 10); ++i) {
          range.push(i);
        }
      }
      Vue.set(state, 'comments', response[2].data.comments);
      Vue.set(state, 'pagesList', range);
      Vue.set(state, 'tags', response[1].data.tags);
      Vue.set(state, 'tag', null);
      Vue.set(state, 'tab', payload[0]);
      Vue.set(state, 'currentPage', 0);
    });
  },

  [HOME_PAGE_LOADED_WITH_RECOMMENDATIONS](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response[0].data.articles);
      Vue.set(state, 'articlesCount', response[0].data.articlesCount);
      const range = [];
      if(response[0].data.articlesCount > 10) {
        for (let i = 0; i < Math.ceil(response[0].data.articlesCount / 10); ++i) {
          range.push(i);
        }
      }
      Vue.set(state, 'comments', response[2].data.comments);
      Vue.set(state, 'pagesList', range);
      Vue.set(state, 'tags', response[1].data.tags);
      Vue.set(state, 'recommendations', response[3].data.articles);
      Vue.set(state, 'tag', null);
      Vue.set(state, 'tab', payload[0]);
      Vue.set(state, 'currentPage', 0);
    });
  },


  [HOME_PAGE_UNLOADED](state) {
    Vue.set(state, 'articles', {});
    Vue.set(state, 'articlesCount', 0);
    Vue.set(state, 'tab', '');
    Vue.set(state, 'pagesList', {});
    Vue.set(state, 'recommendations', []);
    Vue.set(state, 'currentPage', 0);
    Vue.set(state, 'tag', null);
  },

  [CHANGE_TAB](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response.data.articles);
      Vue.set(state, 'articlesCount', response.data.articlesCount);
      const range = [];
      for (let i = 0; i < Math.ceil(response.data.articlesCount / 10); ++i) {
        range.push(i);
      }
      Vue.set(state, 'pagesList', range);
      Vue.set(state, 'tab', payload[0]);
      Vue.set(state, 'currentPage', 0);
      Vue.set(state, 'tag', null);
    });
  },
  [SET_PAGE](state, payload) {
    payload[1].then((response) => {
      Vue.set(state, 'articles', response.data.articles);
      Vue.set(state, 'articlesCount', response.data.articlesCount);
      Vue.set(state, 'currentPage', payload[0]);
    });
  },
  [APPLY_TAG](state, payload) {
    Vue.set(state, 'articles', payload[1].articles);
    Vue.set(state, 'articlesCount', payload[1].articlesCount);
    const range = [];
    for (let i = 0; i < Math.ceil(payload[1].articlesCount / 10); ++i) {
      range.push(i);
    }
    Vue.set(state, 'pagesList', range);
    Vue.set(state, 'tag', payload[0]);
    Vue.set(state, 'tab', null);
    Vue.set(state, 'currentPage', 0)
  },

  [FAVORITE](state, payload) {
    if(state.articles && state.articles.length) {
      for (let article of state.articles) {
        if (article.slug === payload.slug) {
          article.favorited = payload.favorited;
          article.favoritesCount = payload.favoritesCount;
        }
      }
    }
  },

  [UNFAVORITE](state, payload) {
    if(state.articles && state.articles.length) {
      for (let article of state.articles) {
        if (article.slug === payload.slug) {
          article.favorited = payload.favorited;
          article.favoritesCount = payload.favoritesCount;
        }
      }
    }
  },
};
