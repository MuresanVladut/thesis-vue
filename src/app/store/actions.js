/* ============
 * Vuex Actions
 * ============
 *
 * All the actions that can be used
 * inside the store
 */
import * as types from './mutation-types';

// Auth
export const login = ({ commit }, token) => {
  commit(types.LOGIN, token);
};

export const logout = ({ commit }) => {
  commit(types.LOGOUT);
};

export const checkAuthentication = ({ commit }) => {
  commit(types.CHECK_AUTHENTICATION);
};

// Settings

export const settingsSaved = ({ commit }, user) => {
  commit(types.SETTINGS_SAVED, user);
};

export const articleEditor = ({ commit }, payload) => {
  commit(types.ARTICLE_EDITOR, payload);
};

export const articleLoaded = ({ commit }, payload) => {
  commit(types.ARTICLE_PAGE_LOADED, payload);
};

export const deleteComment = ({ commit }, payload) => {
  commit(types.DELETE_COMMENT, payload);
};

export const addComment = ({ commit }, payload) => {
  commit(types.ADD_COMMENT, payload);
};

export const articleDestroyed = ({ commit }) => {
  commit(types.ARTICLE_DESTROYED);
};

export const homePageLoaded = ({ commit }, payload) => {
  commit(types.HOME_PAGE_LOADED, payload);
};

export const homePageLoadedWithRecommendations = ({ commit }, payload) => {
  commit(types.HOME_PAGE_LOADED_WITH_RECOMMENDATIONS, payload);
};

export const homePageUnloaded = ({ commit }) => {
  commit(types.HOME_PAGE_UNLOADED);
};

export const profilePageLoaded = ({ commit }, payload) => {
  commit(types.PROFILE_PAGE_LOADED, payload);
};

export const profilePageUnloaded = ({ commit }) => {
  commit(types.PROFILE_PAGE_UNLOADED);
};

export const setPage = ({ commit }, payload) => {
  commit(types.SET_PAGE, payload);
};

export const setProfilePage = ({ commit }, payload) => {
  commit(types.SET_PAGE_PROFILE, payload);
};

export const changeTab = ({ commit }, payload) => {
  commit(types.CHANGE_TAB, payload);
};

export const favorite = ({ commit }, payload) => {
  commit(types.FAVORITE, payload);
};

export const unfavorite = ({ commit }, payload) => {
  commit(types.UNFAVORITE, payload);
};

export const applyTag = ({ commit }, payload) => {
  commit(types.APPLY_TAG, payload);
};

export const toggleFollowing = ({ commit }, payload) => {
  commit(types.TOGGLE_FOLLOWING, payload);
};

export const toggleFollowingProfile = ({ commit }, payload) => {
  commit(types.TOGGLE_FOLLOWING_PROFILE, payload);
};


export const toggleFavorite = ({ commit }, payload) => {
  commit(types.TOGGLE_FAVORITE, payload);
};

export const changeTabProfile = ({ commit }, payload) => {
  commit(types.CHANGE_TAB_PROFILE, payload);
};
