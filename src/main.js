/* ============
 * Main File
 * ============
 *
 * Will initialize the application
 */
import Vue from 'vue';
import * as App from './app';

require('./bootstrap');
Vue.use(require('vue-moment'));

new Vue(App).$mount('#app');
